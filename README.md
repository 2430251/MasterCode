## About repository

The repository contains the codes used to model data related to the master's thesis, which is entitled "PREDICTING VISITORS’ SPENDINGS: A COMPARISON OF SELECTED REGRESSION METHODS".

The "Data" contains pickle files with data used in work. There are further applied techniques such as One Hot Encoding, MCA and Min-Max Scalar.
In the "Models" folder there is one file containing the structure of all models and 3 additional files containing information on how to prepare the batch data.
